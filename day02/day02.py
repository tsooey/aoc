input = open('input').readlines()[0].split(',')
data = [int(line) for line in input]


def p1(data, noun=12, verb=2):
    data = data.copy()
    data[1], data[2] = noun, verb
    i = 0
    while data[i] != 99:
        src_a, src_b, dest = data[i + 1], data[i + 2], data[i + 3]
        if data[i] == 1:
            data[dest] = data[src_a] + data[src_b]
        elif data[i] == 2:
            data[dest] = data[src_a] * data[src_b]
        i += 4
    return data[0]


def p2(target):
    for noun in range(0, 100):
        for verb in range(0, 100):
            if p1(data, noun, verb) == target:
                return noun * 100 + verb


print(f'part 1: {p1(data)}')
print(f'part 2: {p2(19690720)}')
