open Core

let rec take n xs =
  if n > 0 then
    match xs with
    | [] -> []
    | y :: ys -> y :: (take (n-1) ys)
  else []

let rec drop n = function
  | [] -> []
  | x :: xs -> if n = 0 then (x::xs) else drop (n-1) xs

let replace_with v i xs = (take i xs) @ [v] @ (drop (i+1) xs)

let rec nth i = function
  | [] -> failwith "out of bounds"
  | x :: xs -> if i = 0 then x else nth (i-1) xs

let p1 xs n v =
  let ys = replace_with n 1 xs
  in
  let zs = replace_with v 2 ys
  in
  let rec aux xs i =
    let [op; src_a; src_b; dest] = 
      take 4 (drop i xs)
    in
    match op with
    | 1 -> 
      let v = (nth src_a xs) + (nth src_b xs)
      in
      aux (replace_with v dest xs) (i+4)
    | 2 -> 
      let v = (nth src_a xs) * (nth src_b xs)
      in
      aux (replace_with v dest xs) (i+4)
    | 99 -> nth 0 xs
    | _ -> failwith "not suppose to happen"
  in
  aux zs 0

let p2 xs tgt =
  let rec aux n v =
    if p1 xs n v = tgt then
      100 * n + v
    else
      match n, v with 
      | 99, 99 -> failwith "target not found"
      | n, 99 -> aux (n+1) 0
      | n, v -> aux n (v+1)
  in
  aux 0 0

let () =
  let xs = 
    In_channel.read_all "input"
    |> String.split_on_chars ~on:[',']
    |> List.map ~f:Int.of_string
  in
  Printf.printf "part 1: %i\n" (p1 xs 12 2);
  Printf.printf "part 2: %i\n" (p2 xs 19690720)
