input = open('input').read().split('\n')
data = [line.split(')') for line in input]


def p1(data):
    d = {}
    for [left, right] in data:
        d[right] = left
    count = 0
    for right in d:
        while right in d:
            count += 1
            right = d[right]
    return count


def p2(data):
    d = {}
    for [left, right] in data:
        d[right] = left
    s_you, s_san = set(), set()
    you, san = 'YOU', 'SAN'
    while you in d:
        you = d[you]
        s_you.add(you)
    while san in d:
        san = d[san]
        s_san.add(san)
    return len(s_you ^ s_san)


print(f'part 1: {p1(data)}')
print(f'part 2: {p2(data)}')
