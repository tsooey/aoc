open Core

(* int -> int -> int list *)
let (--) lo hi =
  let rec aux lo hi acc =
    if lo > hi then acc
    else aux lo (hi-1) (hi::acc)
  in
  aux lo hi []

(* int -> bool *)
let inc n =
  let rec aux n init=
    if n = 0 then true
    else 
      let x = n % 10
      in      
      if x <= init then aux (n / 10) x
      else false
  in
  aux n 9

(* int -> bool *)
let repeats n =
  if n < 10 then false
  else
    let init = n % 10
    in
    let rec aux n init =
      if n = 0 then false
      else
        let x = n % 10
        in
        if x = init then true
        else aux (n/10) x
    in
    aux (n/10) init

(* int -> bool *)
let dubs n =
  let digits n =
    let rec aux acc n =
      if n < 10
        then n::acc
      else aux ((n % 10)::acc) (n / 10) in
    aux [] n
  in
  digits n
  |> List.fold ~init:(Map.empty (module Int))
    ~f:(fun acc x -> 
      if Map.mem acc x 
        then Map.set acc ~key:x ~data:((Map.find_exn acc x)+1)
      else Map.set acc ~key:x ~data:1)
  |> Map.data
  |> (fun ls -> List.mem ls 2 ~equal:(=)) && repeats n


(* int -> int -> int *)
let p1 lo hi =
  let xs = lo -- hi
  in
  List.filter ~f:(fun x -> inc x && repeats x) xs
  |> List.length

(* int -> int -> int *)
let p2 lo hi =
  let xs = lo -- hi
  in
  List.filter ~f:(fun x -> inc x && repeats x && dubs x) xs
  |> List.length

let () =
  let lo = 193651
  in
  let hi = 649729
  in
  Printf.printf "part 1: %d\n" (p1 lo hi);
  Printf.printf "part 2: %d\n" (p2 lo hi)
