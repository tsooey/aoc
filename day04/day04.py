from collections import Counter, defaultdict


def p1(lo, hi):
    c = 0
    nums = [str(n) for n in range(lo, hi + 1)]
    for n in nums:
        dubs = False
        inc = True
        for i in range(len(n) - 1):
            if n[i] > n[i + 1]:
                inc = False
                break
            if n[i] == n[i + 1]:
                dubs = True
        if dubs and inc:
            c += 1
    return c


def p2(lo, hi):
    c = 0
    nums = [str(n) for n in range(lo, hi + 1)]
    for n in nums:
        d = defaultdict(int)
        dubs = False
        inc = True
        for i in range(len(n) - 1):
            if n[i] > n[i + 1]:
                inc = False
                break
            if n[i] == n[i + 1]:
                dubs = True
        if dubs and inc and 2 in Counter(n).values():
            c += 1
    return c


print(f'part 1: {p1(193651, 649729)}')
print(f'part 2: {p2(193651, 649729)}')
