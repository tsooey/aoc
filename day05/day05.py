input = open('input').readlines()[0].split(',')
data = [int(num) for num in input]


def p1(data):
    data = data.copy()
    i = 0
    while True:
        op = data[i] % 100
        if op == 1:
            mode_src_a, mode_src_b, mode_dest = data[i] // 100 % 10, data[i] // 1000 % 10, data[i] // 10000 % 10
            src_a = i + 1 if mode_src_a else data[i + 1]
            src_b = i + 2 if mode_src_b else data[i + 2]
            dest = i + 3 if mode_dest else data[i + 3]
            data[dest] = data[src_a] + data[src_b]
            i += 4
        elif op == 2:
            mode_src_a, mode_src_b, mode_dest = data[i] // 100 % 10, data[i] // 1000 % 10, data[i] // 10000 % 10
            src_a = i + 1 if mode_src_a else data[i + 1]
            src_b = i + 2 if mode_src_b else data[i + 2]
            dest = i + 3 if mode_dest else data[i + 3]
            data[dest] = data[src_a] * data[src_b]
            i += 4
        elif op == 3:
            mode_dest = data[i] // 100 % 10
            dest = i + 1 if mode_dest else data[i + 1]
            data[dest] = 1
            i += 2
        elif op == 4:
            mode_dest = data[i] // 100 % 10
            dest = i + 1 if mode_dest else data[i + 1]
            res = data[dest]
            i += 2
            print(res)
        elif op == 99:
            print('halt')
            return None
        else:
            print('something went very wrong')
            return None


def p2(data):
    data = data.copy()
    i = 0
    while True:
        op = data[i] % 100
        if op == 1:
            mode_src_a, mode_src_b, mode_dest = data[i] // 100 % 10, data[i] // 1000 % 10, data[i] // 10000 % 10
            src_a = i + 1 if mode_src_a else data[i + 1]
            src_b = i + 2 if mode_src_b else data[i + 2]
            dest = i + 3 if mode_dest else data[i + 3]
            data[dest] = data[src_a] + data[src_b]
            i += 4
        elif op == 2:
            mode_src_a, mode_src_b, mode_dest = data[i] // 100 % 10, data[i] // 1000 % 10, data[i] // 10000 % 10
            src_a = i + 1 if mode_src_a else data[i + 1]
            src_b = i + 2 if mode_src_b else data[i + 2]
            dest = i + 3 if mode_dest else data[i + 3]
            data[dest] = data[src_a] * data[src_b]
            i += 4
        elif op == 3:
            mode_dest = data[i] // 100 % 10
            dest = i + 1 if mode_dest else data[i + 1]
            data[dest] = 5
            i += 2
        elif op == 4:
            mode_dest = data[i] // 100 % 10
            dest = i + 1 if mode_dest else data[i + 1]
            res = data[dest]
            i += 2
            print(res)
        elif op == 5:
            mode_src_a, mode_src_b = data[i] // 100 % 10, data[i] // 1000 % 10
            src_a = i + 1 if mode_src_a else data[i + 1]
            src_b = i + 2 if mode_src_b else data[i + 2]
            if data[src_a] != 0:
                i = data[src_b]
            else:
                i += 3
        elif op == 6:
            mode_src_a, mode_src_b = data[i] // 100 % 10, data[i] // 1000 % 10
            src_a = i + 1 if mode_src_a else data[i + 1]
            src_b = i + 2 if mode_src_b else data[i + 2]
            if data[src_a] == 0:
                i = data[src_b]
            else:
                i += 3
        elif op == 7:
            mode_src_a, mode_src_b, mode_dest = data[i] // 100 % 10, data[i] // 1000 % 10, data[i] // 10000 % 10
            src_a = i + 1 if mode_src_a else data[i + 1]
            src_b = i + 2 if mode_src_b else data[i + 2]
            dest = i + 3 if mode_dest else data[i + 3]
            data[dest] = 1 if data[src_a] < data[src_b] else 0
            i += 4
        elif op == 8:
            mode_src_a, mode_src_b, mode_dest = data[i] // 100 % 10, data[i] // 1000 % 10, data[i] // 10000 % 10
            src_a = i + 1 if mode_src_a else data[i + 1]
            src_b = i + 2 if mode_src_b else data[i + 2]
            dest = i + 3 if mode_dest else data[i + 3]
            data[dest] = 1 if data[src_a] == data[src_b] else 0
            i += 4
        elif op == 99:
            print('halt')
            return None
        else:
            print('something went very wrong')
            return None


print(f'part 1: {p1(data)}')
print(f'part 2: {p2(data)}')
