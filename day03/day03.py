from collections import defaultdict

input = open('input').readlines()
data = [line.strip() for line in input]
wire_a = [[path[0], int(path[1:])] for path in data[0].split(',')]
wire_b = [[path[0], int(path[1:])] for path in data[1].split(',')]


def dist(p1, p2):
    x0, y0 = p1
    x1, y1 = p2
    return abs(x0 - x1) + abs(y0 - y1)


def p1(wire_a, wire_b):
    xs = []
    pts = set()
    dirs = {
        'R': (1, 0),
        'L': (-1, 0),
        'U': (0, 1),
        'D': (0, -1)
    }
    pos = (0, 0)
    for seg in wire_a:
        dir, n = seg
        vec = dirs[dir]
        for _ in range(n):
            pos = tuple(map(sum, zip(pos, vec)))
            pts.add(pos)
    pos = (0, 0)
    for seg in wire_b:
        dir, n = seg
        vec = dirs[dir]
        for _ in range(n):
            pos = tuple(map(sum, zip(pos, vec)))
            if pos in pts:
                xs.append(pos)
    return min([dist(x, (0, 0)) for x in xs])


def p2(wire_a, wire_b):
    xs = []
    pts = defaultdict(int)
    dirs = {
        'R': (1, 0, 1),
        'L': (-1, 0, 1),
        'U': (0, 1, 1),
        'D': (0, -1, 1)
    }
    pos = (0, 0, 0)
    for path in wire_a:
        dir, n = path
        vec = dirs[dir]
        for _ in range(n):
            pos = tuple(map(sum, zip(pos, vec)))
            x, y, t = pos
            pts[x, y] += t
    pos = (0, 0, 0)
    for path in wire_b:
        dir, n = path
        vec = dirs[dir]
        for _ in range(n):
            pos = tuple(map(sum, zip(pos, vec)))
            x, y, t = pos
            if (x, y) in pts:
                xs.append(t + pts[x, y])
    return min(xs)


print(f'part 1: {p1(wire_a, wire_b)}')
print(f'part 2: {p2(wire_a, wire_b)}')
