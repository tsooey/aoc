input = open('input').readlines()
data = [int(line) for line in input]


def p1(data):
    res = 0
    for n in data:
        res += n // 3 - 2
    return res


def p2(data):
    res = 0
    for n in data:
        while n > 0:
            res += max(n // 3 - 2, 0)
            n = n // 3 - 2
    return res


print(f'part 1: {p1(data)}')
print(f'part 2: {p2(data)}')
