open Core

let fuel x = 
  x / 3 - 2

let rec ffuel x =
  match fuel x > 0 with
  | true -> fuel x + ffuel (fuel x)
  | false -> 0

let p1 xs =
  xs
  |> List.map ~f:fuel
  |> List.fold ~init:0 ~f:(+)

let p2 xs = 
  xs
  |> List.map ~f:ffuel
  |> List.fold ~init:0 ~f:(+)
 
let () =
  let xs = 
    In_channel.read_lines "input"
    |> List.map ~f:Int.of_string
  in
  Printf.printf "part 1: %i\n" (p1 xs);
  Printf.printf "part 2: %i\n" (p2 xs)