from collections import defaultdict
from math import ceil, floor
import re


input = open('input')
data = [[n.strip() for n in re.findall(r'-?\d+|[A-Z]+', line)]
        for line in input]


def p1(data, fuel):
    reaction = defaultdict(list)
    for formula in data:
        reactant = list(map(lambda x: (int(x[0]), x[1]), zip(
            formula[:-2:2], formula[1:-2:2])))
        product = (int(formula[-2]), formula[-1])
        n, name = product
        reaction[name].extend([n, reactant])
    need, excess = defaultdict(int), defaultdict(int)
    need['FUEL'] = fuel

    def get_ore(need):
        for (prod, amount) in need.items():
            if prod == 'ORE':
                continue
            else:
                if excess[prod] >= amount:
                    excess[prod] -= amount
                    amount = 0
                elif excess[prod] < amount:
                    amount -= excess[prod]
                    excess[prod] = 0
                n, reactant = reaction[prod]
                m = ceil(amount / n)
                excess[prod] = m * n - amount
                for (k, r) in reactant:
                    need[r] += m * k
            del need[prod]
            return get_ore(need)
    get_ore(need)
    return need['ORE']


def p2(data, ore_amount):
    lo, hi = 0, 999999999
    while lo + 1 < hi:
        mid = lo + ((hi - lo) // 2)
        x = p1(data, mid)
        if x > ore_amount:
            hi = mid - 1
        elif x < ore_amount:
            lo = mid
    return lo


print(f'part 1: {p1(data, 1)}')
print(f'part 2: {p2(data, 1000000000000)}')
