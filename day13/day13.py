from collections import defaultdict
from os import system
from time import sleep


input = open('input').read().split(',')
data = [int(n) for n in input]


def intcode(prog, prog_in):
    data = defaultdict(int)
    for i, n in enumerate(prog):
        data[i] = n
    base = 0
    i = 0

    def args(n):
        res = []
        mode_src_a = data[i] // 100 % 10
        mode_src_b = data[i] // 1000 % 10
        mode_src_c = data[i] // 10000 % 10
        if n > 0:
            src_a = base + \
                data[i + 1] if mode_src_a == 2 else (i + 1 if mode_src_a else data[i + 1])
            res.append(src_a)
        if n > 1:
            src_b = base + \
                data[i + 2] if mode_src_b == 2 else (i + 2 if mode_src_b else data[i + 2])
            res.append(src_b)
        if n > 2:
            src_c = base + \
                data[i + 3] if mode_src_c == 2 else (i + 3 if mode_src_c else data[i + 3])
            res.append(src_c)
        return res

    while True:
        op = data[i] % 100
        if op == 1:
            [src_a, src_b, src_c] = args(3)
            data[src_c] = data[src_a] + data[src_b]
            i += 4
        elif op == 2:
            [src_a, src_b, src_c] = args(3)
            data[src_c] = data[src_a] * data[src_b]
            i += 4
        elif op == 3:
            [src_a] = args(1)
            data[src_a] = prog_in.pop()
            i += 2
        elif op == 4:
            [src_a] = args(1)
            res = data[src_a]
            yield res
            i += 2
        elif op == 5:
            [src_a, src_b] = args(2)
            if data[src_a] != 0:
                i = data[src_b]
            else:
                i += 3
        elif op == 6:
            [src_a, src_b] = args(2)
            if data[src_a] == 0:
                i = data[src_b]
            else:
                i += 3
        elif op == 7:
            [src_a, src_b, src_c] = args(3)
            data[src_c] = 1 if data[src_a] < data[src_b] else 0
            i += 4
        elif op == 8:
            [src_a, src_b, src_c] = args(3)
            data[src_c] = 1 if data[src_a] == data[src_b] else 0
            i += 4
        elif op == 9:
            [src_a] = args(1)
            base += data[src_a]
            i += 2
        elif op == 99:
            yield None
        else:
            print('something went very wrong')
            return None


def render(d, xmax=43, ymax=23):
    res = ''
    for y in range(ymax + 1):
        for x in range(xmax + 1):
            tile = d[(x, y)]
            res += ' ' if tile == 0 else \
                   '|' if tile == 1 else \
                   '@' if tile == 2 else \
                   '=' if tile == 3 else \
                   'o'
        res += '\n'
    print(res)
    sleep(0.001)
    system('clear')


def p1(data):
    d = defaultdict(set)
    g = intcode(data, [1])
    while True:
        x = next(g)
        if x is None:
            return len(d[2])
        y = next(g)
        tile = next(g)
        d[tile].add((x, y))


def p2(data):
    data[0] = 2
    d = defaultdict(int)
    inp = []
    g = intcode(data, inp)
    xmax, ymax = 0, 0
    ball, paddle, score = None, None, 0
    while True:
        x = next(g)
        if x is None:
            return score
        y, tile = next(g), next(g)
        d[(x, y)] = tile
        # render(d)
        if x == -1 and y == 0:
            score = tile
            # print(f'score: {score}')
        else:
            if tile == 3:
                paddle = x
            elif tile == 4:
                ball = x
            if paddle and ball:
                del inp[:]
                inp.insert(
                    0, -1 if ball < paddle else 0 if ball == paddle else 1)


print(f'part 1: {p1(data)}')
print(f'part 2: {p2(data)}')
